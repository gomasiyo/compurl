<?php
/**
 *  getURL system
 *
 *  @varsion 1.0
 *  @author Goma.Nanoha
 *  Copyright 2013 Geekz Web Development
*/
require_once "../../config/config.php";
require_once "../../class/database.php";
require_once "../../class/url.php";

DataBase::Connect($conf);

    $url = new Url($_GET);
    $url->getUrl();

DataBase::DisConnect();

?>
