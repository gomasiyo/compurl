<?php
/**
 *  getServiceList system
 *
 *  @varsion 1.0
 *  @author Goma.Nanoha
 *  Copyright 2013 Geekz Web Development
*/

// ServiceList Address
$servicelist = 'http://pcinfo.geekz-dev.net/about/about.xml';

// Json Header
header("Content-Type: application/json; charset=utf-8");

$xml = simplexml_load_file($servicelist);
$json = json_encode($xml);

echo $json;

?>
