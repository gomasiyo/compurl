<?php
/**
 *  goURL system
 *
 *  @varsion 1.0
 *  @author Goma.Nanoha
 *  Copyright 2013 Geekz Web Development
 */

$flag = $_GET[ 'flag' ];

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ) {
    $protocol = 'https://';
} else {
    $protocol = 'http://';
}


header( 'Location:'. $protocol.$_SERVER['HTTP_HOST'] ."#url/$flag");

?>
