$(function() {

    var surl = './api/surl.php',
        gurl = './api/gurl.php',
        glist = './api/glist.php';

    var d = document,
        w = window;

    var getHash = function () {
        return location.hash.substr( 1 ).split( '/' );
    }

    var router = {

        loadPage : function () {
            var hash = getHash();
            page = this.pageList( hash[ 0 ] );


            if( page == 'url' ) {
                if( hash[ 1 ] != null ) return this.moveUrl( hash[ 1 ] );
                return view.error( 'アドレスが指定されていません' );
            }

            ajaxModel.loadPage( page ).done( function ( content ) {

                view.main( content );

                // CompURLシステム
                if( page == 'comp' ) return comp.start();

            }).fail( function ( XHR, status, errorThrown ) {
                view.error( errorThrown );
            });
        },

        pageList : function ( hash ) {
            var list = new Array( 'comp', 'decomp', 'api', 'url');
            if( hash == '' ) return 'comp';
            for ( i = 0; i < list.length; i++ ) {
                if( hash == list[ i ] ) return hash;
            }

            return 'comp';
        },

        moveUrl : function ( hash ) {

            ajaxModel.getUrl( hash ).done( function ( status ) {

                if( status.Status ) {
                    return location.href = status.URL;
                } else {
                    if( status.Massage == 'Url Empty' ) return view.error( 'アドレスが入っていません' );
                    if( status.Massage == 'No Url' ) return view.error( 'アドレスが保存されていません' );

                    return view.error( '不明なエラーです' );
                }

            }).fail( function ( XHR, status, errorThrown) {
                view.error( errorThrown );
            });

        },

        setFotter: function () {
            ajaxModel.getList().done( function ( contents ) {
                $.each( contents.link, function( key, val ) {
                    view.setList( val.title, val.url );
                });

            }).fail( function ( XHR, status, errorThrown) {
                view.error( errorThrown );
            });
        }

    }

    var ajaxModel = {

        loadPage : function ( page ) {
            var defer = $.Deferred();

            $.ajax({
                url : './contents/' + page + '.html',
                dataType : 'html',
                cache : false,
                success : defer.resolve,
                error : defer.reject
            });

            return defer.promise();
        },

        setUrl : function ( val ) {
            var defer = $.Deferred();

            $.ajax({
                url : surl,
                dataType : 'json',
                data : {
                    url : val
                },
                cache : false,
                success : defer.resolve,
                error : defer.reject
            });

            return defer.promise();
        },

        getUrl : function ( hash ) {
            var defer = $.Deferred();

            $.ajax({
                url : gurl,
                dataType : 'json',
                data : {
                    url : hash,
                },
                cache : false,
                success :defer.resolve,
                error : defer.reject
            });

            return defer.promise();

        },

        getList : function () {
            var defer = $.Deferred();

            $.ajax({
                url : glist,
                dataType : 'json',
                cache : false,
                success : defer.resolve,
                error : defer.reject
            });

            return defer.promise();
        }

    }

    var view = {

        init : function () {
            $( '#main_section' ).fadeOut( 'fast' ).promise().done( function() {
                $( '#main_section' ).empty();
                router.loadPage();
            });
        },

        main : function ( content ) {
            $( '#main_section' ).append( content );
            $( '#main_section' ).fadeIn( 'fast' );
        },

        compurl : function ( url, data ) {
            var box = $( '<div />', { id : 'compurl_seturl_'+ data , class : 'lightbox' });
            var cross = $( '<div />', { id : 'compurl_cross_'+ data, class : 'cross', text : '×' });
            var url = $( '<span />', { id : 'compurl_url' +data, class : 'lightbox_left', text : url });

            box.prepend( url );
            box.append( cross );

            $( '#compurl_set' ).prepend( box );
            $( '#compurl_seturl_'+ data ).fadeIn( 'fast' );
        },

        setList : function ( title, url ) {
            var list = $( '<li />' );
            var link = $( '<a />', { href : url, text : title });

            list.append( link );

            $( '#footer_main > ul' ).append( list );
        },

        remove : function ( obj ) {
            var defer = $.Deferred();

            obj.fadeOut( 'fast' ).promise().done( function() {
                obj.remove();
                defer.resolve();
            });

            return defer.promise();
        },

        error : function ( status ) {
            var box = $( '<div />', { id : 'error_message', class : 'lightbox' });
            var message = $( '<span />', { class : 'lightbox_left', text : status } );
            box.append( message );

            // エラー表示
            $( '#main_section' ).append( box );
            $( '#main_section' ).show();
            $( '#error_message' ).fadeIn( 'fast' );

            // エラー非表示
            setTimeout( function () {
                $( '#error_message' ).fadeOut( 'fast' ).promise().done( function() {
                    $( '#error_message' ).remove();
                });
            }, 1000);

        }

    }

    var comp = {

        url : {},

        start : function () {
            this.clickSubmit();
            this.enterSubmit();
            this.focus();
            this.cross();
        },

        submit : function () {

            var val = $( '#compurl_input' ).val();

            // エラー項目
            if( val == '' ) return view.error( 'フォームが入力されていません' );
            if( comp.url == val ) return view.error( '二重投稿は禁止されています' );

            ajaxModel.setUrl( val ).done( function ( Callback ) {

                if( Callback.Status ) {

                    var urlNum = $( '.lightbox' ).length;
                    view.compurl( Callback.URL, urlNum );

                } else {

                    $.each( Callback.Massage , function( key, val ) {
                        if( val == 'Url Empty' ) {
                            view.error( 'URLが入力されていません' );
                            return false;
                        }
                        if( val == 'Url Invalid' ) view.error( 'URLの形式が不明です' );
                    });

                }

                // 二重投稿防止
                comp.url = val;

            }).fail( function ( XHR, status, errorThrown ) {
                view.error( errorThrown );
            });

        },

        clickSubmit : function () {
            $( '#compurl_submit' ).click( function() {
                comp.submit();
            });
        },

        enterSubmit : function () {
            $( '#article_compurl' ).keypress( function( e ) {
                if( e.which && e.which == 13 ) {
                    // submit イベント停止
                    e.preventDefault();
                    comp.submit();
                }
            });
        },

        focus : function () {
             $( d ).on( 'click', '.lightbox_left', function () {
                $( this ).select();
            });
        },

        cross : function () {
            $( d ).on( 'click', '.cross', function () {
                view.remove( $( this ).parent( 'div' ) );
            });
        }

    }

    // HUSH監視
    tm.HashObserver.enable();
    d.addEventListener( 'changehash', function( e ) {
        view.init();
    }, false);

    // フッターサービスリスト作成
    router.setFotter();

    view.init();

});
