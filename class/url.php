<?php
/**
 *  Url Class
 *
 *  @varsion 1.5
 *  @author Goma.Nanoha <goma@goma-gz.net>
 *  Copyright 2013 Geezk Web Development
*/

final class Url
{

    // System Main
    private $Db;
    private $Status;

    // System Var
    private $Rand;
    private $ErrorCode;
    private $CheckStatus;

    // User
    private $Url;
    private $Time;
    private $Kill;
    private $Type;
    private $Callbace;

    // Config
    private $randNum = 4;


    public function __construct($get)
    {

        // DataBase Connect
        $this->Db = DataBase::Singleton();

        // Get var
        $this->Url = $get['url'];
        $this->Time = $get['time'];
        $this->Kill = $get['kill'];
        $this->Type = $get['type'];
        $this->Callback = $get['callback'];



    }

    // Header Setting
    public function getHeader()
    {

        if($this->Type == 'jsonp') {
            header('Content-type: text/javascript');
        } else {
            header('Content-Type: application/json; charset=utf-8');
        }

    }

    // Status Output
    public function outStatus()
    {

        if($this->Type == 'jsonp') {
            echo $this->Callback . "(" . json_encode($this->Status) . ")";
        } else {
            echo json_encode($this->Status);
        }

    }

    // get NowURL
    public function getNowUrl()
    {

        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }

        return $protocol . $_SERVER['HTTP_HOST'];

    }

    // Random URL
    private function randUrl()
    {

        $char = array();

        $char[] = range(0, 9);
        $char[] = range('a', 'z');
        $char[] = range('A', 'Z');

        $list = array();
        $merge = array();
        $count = 0;

        foreach($char as $arr) {
            shuffle($arr);
            $list[] = $arr[0];
            $merge = array_merge($merge, $arr);
            $count++;
        }

        $flip = array_flip($merge);
        while($count < $this->randNum) {
            $list[] = array_rand($flip, 1);
            $count++;
        }
        shuffle($list);

        $this->Rand = implode("", $list);

    }

    // Check URL
    private function checkUrl($status)
    {

        // Check Empty
        if(empty($this->Url)) {
            $this->ErrorCode[] = "Url Empty";
        }

        // Check Regular And Check Overlap
        if($status == 'set') {

            // Check Regular
            $regularStatus;

            $pattern = array(
                '/^https?:\/\/(.*)/',    // HTTP OR HTTPS
                '/^ftp:\/\/(.*)/'       // FTP
            );

            foreach($pattern as $val) {
                if(preg_match($val, $this->Url)) {
                    $regularStatus[] = 1;
                }
            }

            if(empty($regularStatus)) {
                $this->ErrorCode[] = "Url Invalid";
            } else {

                // Check Overap
                $token = $this->modelGetUrl($this->Rand);
                if(!empty($token)) $this->setUrl();

            }

        }

        // Check OneKill And TimeKill
        if(empty($this->Kill)) $this->Kill = 0;
        if(empty($this->Time)) $this->Time = 0;

        // Check Callback
        if(empty($this->Callback)) $this->Callback = "callback";

        // True or False Massage
        if(empty($this->ErrorCode)) {
            $this->CheckStatus = true;
        } else {

            // False Massage
            $this->Status = array(
                'Status' => false,
                'Massage' => $this->ErrorCode
            );

            $this->CheckStatus = false;

        }


    }

    // Check KillURL
    private function checkKillUrl($array)
    {

        // Check KillStatus
        $killStatus;

        // Check One Kill
        if($array[0]['kill'] != 0) $killStatus[] = 1;

        // Check Time Kill
        if($array[0]['time'] != 0) {
            if(time() > $array[0]['time']) $killStatus[] = 1;
        }

        // Return true or False
        if(!empty($killStatus)) {
            return true;
        } else {
            return false;
        }

    }

    // Model GetURL
    private function modelGetUrl($token) {

        $sql = 'SELECT * FROM url WHERE `token` = ? ORDER BY `id` DESC';
        $stmt = $this->Db->prepare($sql);
        $stmt->execute(array(
            $token
        ));

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    // Model SetURL
    private function modelSetUrl($array) {

        $sql = 'INSERT INTO url (`url`, `token`, `time`, `kill`) VALUES (?, ?, ?, ?)';
        $stmt = $this->Db->prepare($sql);
        $flag = $stmt->execute($array);

        return $flag;

    }

    // Get URL
    public function getUrl()
    {

        // Check URL
        $this->checkUrl('get');

        if($this->CheckStatus) {

            // URL Data Get
            $flag = $this->modelGetUrl($this->Url);

            if(!empty($flag)) {

                $this->Status = array(
                    'Status' => true,
                    'URL' => $flag[0]['url']
                );

                // Check Kill URL
                if($this->checkKillUrl($flag)) {
                    // Dell URL
                    $this->Status = $flag;
                }

            } else {
                $this->Status = array(
                    'Status' => false,
                    'Massage' => 'No Url'
                );
            }

        }

        // Output Status
        $this->getHeader();
        $this->outStatus();

    }

    public function setUrl()
    {

        // RandomURL and CheckURL
        $this->randUrl();
        $this->checkUrl('set');

        if($this->CheckStatus) {

            // SQL Data
            $sql = array(
                $this->Url,
                $this->Rand,
                $this->Time,
                $this->Kill
            );

            // URL Data Set
            $flag =$this->modelSetUrl($sql);

            if($flag) {
                $this->Status = array(
                    'Status' => true,
                    'URL' => $this->getNowUrl().'/'.$this->Rand,
                    'Rand' => $this->Rand
                );
            } else {
                $this->Status = array(
                    'Status' => false,
                    'Massage' => 'Insert Error'
                );
            }

        }

        // Output Status
        $this->getHeader();
        $this->outStatus();

    }

}
