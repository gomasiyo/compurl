<?php
/**
 *  Url Class
 *
 *  @varsion 1.2
 *  @author Goma.Nanoha
 *  Copyright 2013 Geekz Web Development
*/
final class Url
{

    private $Url;
    private $Rand;
    private $Db;
    private $Type;
    private $Callback;
    private $Status;
    private $ErrorCode;
    private $checkStatus;
    private $randNum = 4;


    public function __construct($get)
    {

        $this->Url = $get['url'];
        $this->Db = DataBase::Singleton();

        $this->Type = $get['type'];
        $this->Callback = $get['callback'];

    }

    public function getHeader()
    {

        if($this->Type == 'jsonp') {
            header("Content-type: text/javascript");
        } else {
            header("Content-Type: application/json; charset=utf-8");
        }

    }

    public function getStatus()
    {

        if($this->Type == 'jsonp') {
            echo $this->Callback . "(" . json_encode($this->Status) . ")";
        } else {
            echo json_encode($this->Status);
        }

    }

    public function getNowUrl()
    {

        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ) {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }

        return $protocol.$_SERVER['HTTP_HOST'];

    }

    private function randUrl()
    {

        $char = array();

        $char[] = range(0, 9);
        $char[] = range('a', 'z');
        $char[] = range('A', 'Z');

        $list = array();
        $merge = array();
        $count = 0;

        foreach($char as $arr) {
            shuffle($arr);
            $list[] = $arr[0];
            $merge = array_merge($merge, $arr);
            $count++;
        }

        $flip = array_flip($merge);
        while($count < $this->randNum) {
            $list[] = array_rand($flip, 1);
            $count++;
        }
        shuffle($list);

        $this->Rand = implode("", $list);

    }

    private function checkUrl($state)
    {

        if(empty($this->Url)) {
            $this->ErrorCode[] = "Url Empty";
        }

        if($state == 'set') {

            $patternState;

            $pattern = array(
                '/^https?:\/\/(.*)/',    // HTTP OR HTTPS
                '/^ftp:\/\/(.*)/'       // FTP
            );

            foreach($pattern as $val) {

                if(preg_match($val, $this->Url)) {
                    $patternState = 1;
                }

            }

            if(empty($patternState)) {
                $this->ErrorCode[] = "Url Invalid";
            } else {
                $token = $this->modelGetUrl($this->Rand);
                if(!empty($token)) $this->setUrl();
            }

        }

        if(empty($this->Callback)) {
            $this->Callback = "callback";
        }

        if(empty($this->ErrorCode)) {
            $this->checkStatus = true;
        } else {

            $this->Status = array(
                "Status" => false,
                "Massage" => $this->ErrorCode
            );

            $this->checkStatus = false;

        }

    }

    public function setUrl()
    {

        $this->randUrl();
        $this->checkUrl('set');

        if($this->checkStatus) {

            $sql = 'INSERT INTO url (`url`, `token`) VALUES (?, ?)';
            $stmt = $this->Db->prepare($sql);
            $flag = $stmt->execute(array(
                $this->Url,
                $this->Rand
            ));

            if($flag) {

                $this->Status = array(
                    "Status" => true,
                    "URL" => $this->getNowUrl().'/'.$this->Rand,
                    "Rand" => $this->Rand
                );

            } else {

                $this->Status = array(
                    "Status" => false,
                    "Massage" => "Insert Error"
                );

            }

        }

        $this->getHeader();
        $this->getStatus();

    }

    private function modelGetUrl($token) {

        $sql = 'SELECT * FROM url WHERE `token` = ? ORDER BY `id` DESC';
        $stmt = $this->Db->prepare($sql);
        $stmt->execute(array(
            $token
        ));

        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function getUrl()
    {

        $this->checkUrl('get');

        if($this->checkStatus) {

            $flag = $this->modelGetUrl($this->Url);

            if(!empty($flag)) {

                $this->Status = array(
                    "Status" => true,
                    "URL" => $flag[0]['url']
                );

            } else {

                $this->Status = array(
                    "Status" => false,
                    "Massage" => "No Url"
                );

            }

        }

        $this->getHeader();
        $this->getStatus();

    }

}

?>
